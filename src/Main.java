import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Function;
import javax.sound.sampled.AudioFileFormat.Type;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.FileNotFoundException;
import org.json.*;

class Rule{
	String name; 
	int direction;
	Boolean Precondition (Node state){
		if (this.direction == 1 && (state.blankLocation > state.dimension -1)) {
			return true;
		}
		else if (this.direction == 2 && state.blankLocation < (state.board.size() - state.dimension)) {
			return true; 
		}
		else if (this.direction == 3 && (state.blankLocation) % state.dimension != 0) {
			return true; 
		}
		else if (this.direction == 4 && (state.blankLocation + 1) % state.dimension != 0) {
			return true; 
		}		
		return false; 
	}
	Rule(String name, int direction){
		this.direction = direction; 
		this.name = name; 
	}
	Rule(Rule old){
		this.direction = old.direction; 
		this.name = old.name;
	}
	
	Node action (Node state,ArrayList<Rule> rules) {
		int tmp = 0;
		int index = 0;
		int index2 = 0; 
		Node x = new Node(state);
		// returns a node with the new state being x
		if (this.Precondition(state)) {

			if (this.direction == 1 && (state.blankLocation > state.dimension -1)) {
				// move up
				index = (state.board.indexOf(0) - state.dimension);  
				state.up = x; 				
			}
			else if (this.direction == 2 && state.blankLocation < (state.board.size() - state.dimension)) {
				//move down
				index = (state.board.indexOf(0) + state.dimension);
				state.down = x; 
			}
			else if (this.direction == 3 && (state.blankLocation) % state.dimension != 0) {
				// move left
				index = (state.board.indexOf(0) - 1); 
				state.left = x; 
			}
			else if (this.direction == 4 && (state.blankLocation + 1) % state.dimension != 0) {
				// move right
				index = (state.board.indexOf(0) + 1); 
				state.right = x; 
			}
			x.parent = state; 
			// find blank
			index2 = (state.board.indexOf(0)); 
			// get variable of swap number
			tmp = x.board.get(index);
			// swap values
			x.board.set(index2, x.board.get(index)); 
			x.board.set(index, 0); 
			x.blankLocation = index;
			x.applicable_Rules(rules);
			
			x.moves.add(this);
			
			// remove this rule from applicable rule list 
			// since we just applied this rule and went 
			// this direction
			x.popRule(this);
			// increment the move counter
			x.moveCount += 1 ;
			return x; 
		}
		return null;
	}
	
	public <T> void puts (T arg) {
		System.out.println(arg);
	}
}

class Node implements Comparable<Node>{
	Node up;
	Node down;
	Node left;
	Node right;
	Node parent;
	int blankLocation;
	ArrayList<Rule> moves; 
	ArrayList<Integer> board;
	ArrayList<Integer> goal;
	ArrayList<Rule> applicable_Rules_list; 
	int moveCount; 
	int dimension; 
	int heuristics;
	// remove the opposite of rule to ensure no backtracking
	void popRule(Rule rem){
		for (Rule rule : this.applicable_Rules_list) {

			if (rule.name == "up" && rem.name  == "down") {
				this.applicable_Rules_list.remove(rule);
				return; 
			}
			else if (rule.name == "down" && rem.name  == "up") {
				this.applicable_Rules_list.remove(rule);
				return; 			
			}
			else if (rule.name == "left" && rem.name  == "right") {
				this.applicable_Rules_list.remove(rule);
				return; 
			}
			else if (rule.name == "right" && rem.name  == "left") {
				this.applicable_Rules_list.remove(rule);
				return; 
			}		
		}
	}
	Boolean solved() {
		if (this.goal == null) {return false;}
		for(int i = 0 ; i < this.board.size(); ++i) {
			if (this.board.get(i) != this.goal.get(i)) {
				return false; 
			}
		}
		return true; 
	}
	Boolean equal(Node obj) {
		if (this.board == null || obj.board == null) {return false;}
		for(int i = 0 ; i < this.board.size(); ++i) {
			if (this.board.get(i) != obj.board.get(i)) {
				return false; 
			}
		}
		return true; 
	}
	void applicable_Rules(ArrayList<Rule> rules) {
		this.applicable_Rules_list = new ArrayList<Rule>();
		for (int i = 0 ; i < rules.size(); ++i) {
			if (rules.get(i).Precondition(this)){
				this.applicable_Rules_list.add(new Rule(rules.get(i)));
			}
		}
	}
	
	Node readFile(String Filename) {
		File file = new File(Filename);
	    Scanner scan;
	    String str = new String();
	    
		try {
			scan = new Scanner(file);
			while (scan.hasNext())
		        str += scan.nextLine();
		    scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(-1); 
		}
		
	    try {
			JSONObject obj = new JSONObject(str);
			
			if (! (obj.has("n") || obj.has("goal")|| obj.has("start"))) {
				puts("ERROR reading string");
				System.exit(-2);
			}

			int dimension = obj.getInt("n");
			JSONArray array = obj.getJSONArray("goal");
			JSONArray row ; 
			if (dimension <= 0 ) {puts("ERROR reading string"); System.exit(-2);}
			
			for (int i = 0 ; i < array .length(); ++i ) {
				row = (JSONArray) array.get(i);
				if (row.length() > dimension) {//throw error
					puts("ERROR reading string");System.exit(-2);
				}
				for (int j = 0; j < row.length(); ++j) {
					this.goal.add((int)row.get(j));
				}
				if(i > row.length()) {//throw error
						puts("ERROR reading file");System.exit(-2);
				}	
			}
			array = obj.getJSONArray("start");
			for (int i = 0 ; i < array.length(); ++i ) {
				row = (JSONArray) array.get(i);
		
				if (row.length() > dimension) {//throw error
					puts("ERROR reading file");System.exit(-2);
				}
				
				for (int j = 0; j < row.length(); ++j) {
					this.board.add((int)row.get(j));
				}				
			}
			this.dimension = dimension; 			
			if (!(this.board.contains(0) || this.goal.contains(0))) {
				puts("ERROR reading file");
				System.exit(-2);
			}
			if(this.board.size() != this.goal.size()) {
				puts("ERROR reading file");
				System.exit(-2);
			}
			this.blankLocation = this.board.indexOf(0);
		} catch (JSONException e) {
			e.printStackTrace();
			System.exit(-1); 
		}
	    return null;
	}
	int distance() {
		int count = 0; 
		for (int i = 0; i < this.board.size(); ++i) {
			if (this.goal.get(i) != this.board.get(i)) {
				count += 1; 
			}
		}
		return count; 
	}
	int manhattanDistance() {
		int count = 0;
		int boardVert = 0;
		int boardHoriz = 0;
		int goalVert = 0; 
		int goalHoriz = 0; 
		//x / 3 gives vertical distance
		//x % 3 gives horizontal distance
		for (int i = 0; i < this.board.size(); ++i) {
			goalVert = (this.goal.indexOf(this.board.get(i)) / this.dimension);
			goalHoriz = (this.goal.indexOf(this.board.get(i)) % this.dimension);
			boardVert = (this.board.indexOf(this.board.get(i)) / this.dimension);
			boardHoriz = (this.board.indexOf(this.board.get(i)) % this.dimension);	
			count += Math.abs(goalVert - boardVert) + Math.abs(goalHoriz - boardHoriz);
		}
		return count;
	}
	
	Node(Node item){
		this.board = new ArrayList<Integer>(item.board);
		this.goal = new ArrayList<Integer>(item.goal);
		this.moves = new ArrayList<Rule>(item.moves);
		this.applicable_Rules_list = new ArrayList<Rule>(item.applicable_Rules_list);
		this.dimension = item.dimension; 
		this.blankLocation = item.blankLocation;
		this.moveCount = item.moveCount;
		this.heuristics = item.heuristics;
	}
	
	Node(){
		this.applicable_Rules_list= new ArrayList<Rule>();
		
		this.board = new ArrayList<Integer>();
		this.goal = new ArrayList<Integer>();
		this.moves = new ArrayList<Rule>();
		this.dimension = (int) Math.sqrt(this.board.size());
	}
	@Override
	public int compareTo(Node candidate) {
		if (this.heuristics == 1) {
			if (this.distance() + this.moveCount < candidate.distance() + candidate.moveCount) { return -1; } 
			else if (this.distance() + this.moveCount == candidate.distance() + candidate.moveCount) {return 0;}
			else {return 1;} 
		} else if (this.heuristics == 2) {
			if (this.manhattanDistance() + this.moveCount < candidate.manhattanDistance() + candidate.moveCount) { return -1; } 
			else if (this.manhattanDistance() + this.moveCount == candidate.manhattanDistance() + candidate.moveCount) {return 0;}
			else {return 1;} 			
		}
		return 0; 
	}       
	
	@Override
	public String toString(){
		String var = new String();
		for (int i = 0 ; i < this.board.size(); ++i){
			if (this.board.get(i) == 0) {
				var += " , ";
			}
			else {
				if (this.dimension > 3 && this.goal.get(i) <= 9) {
					var += this.board.get(i) + ",  " ; 
				} else {
					var += this.board.get(i) + ", " ; 
				}
			}
			if (i % this.dimension == (this.dimension -1)) {
				var += "\n";
			}
		}		
		return var; 
	}
	public void printGoal(){
		String var = new String();
		for (int i = 0 ; i < this.goal.size(); ++i){
			if (this.goal.get(i) == 0) {
				var += " , ";
			}
			else {
				//var += this.goal.get(i) + ", " ; 
				if (this.dimension > 3 && this.goal.get(i) <= 9) {
					var += this.goal.get(i) + ",  " ; 
				} else {
					var += this.goal.get(i) + ", " ; 
				}
			}
			if (i % this.dimension == (this.dimension -1) ) {
				var += "\n";
			}
		}				
		puts (var); 
	}
	public <T> void puts (T arg) {
		System.out.println(arg);
	}
}

public class Main {
	public static void main(String[] args) {
		System.gc(); 
		int heuristics = 0; 
		String [] files  = {
				"1-move.json", //     0
				"2-moves.json", //    1
				"3-moves.json", //    2
				"4-moves.json", //    3
				"5-moves.json", //    4
				"10-moves.json", //   5 
				"15-moves.json", //   6
				"20-moves.json", //   7
				"25-moves.json", //   8
				"15-puzzle.json", //  9
				"problem-1.json",};// 10
		Integer [] bound = {
				1,2,3,4,5,10,15,20,25,15,
				25};
		
		// simple DFS for set of files (uncomment to run)
		//DFS(files, bound);
		
		// Iterative DFS on files
		//IterativeDFS(files);

//		i < files.length

//		// Graph Search --> (heuristics = 0 )
//		heuristics = 0; 
//		for (int i = 6 ; i < 7; ++i ) {		
//			Node x = new Node();
//			x.readFile(files[i]);	
//			GraphSearch(x, heuristics );
//		}
//		// A* Graph Search --> (heuristics = 1 ) regular Distance

//		heuristics = 1; 
//		for (int i = 10 ; i < 11; ++i ) {		
//			Node x = new Node();
//			x.readFile(files[i]);	
//			GraphSearch(x, heuristics );
//		}
		// A* Graph Search --> (heuristics = 1 ) Manhattan Distance

		heuristics = 2; 
		for (int i = 10 ; i < 11; ++i ) {		
			Node x = new Node();
			x.readFile(files[i]);	
			GraphSearch(x, heuristics );
		}

		Toolkit.getDefaultToolkit().beep();
		Toolkit.getDefaultToolkit().beep();

	}
	
	public static void GraphSearch(Node state, int heuristics )  {
		ArrayList<Rule> orderedRules = new ArrayList<Rule>();
		orderedRules.add(new Rule("up",1));
		orderedRules.add(new Rule("down",2));
		orderedRules.add(new Rule("left",3));
		orderedRules.add(new Rule("right",4));
		
		state.applicable_Rules(orderedRules);
		Boolean check = false; 
		
		ArrayList<Node> open = new ArrayList<Node>(100000);
		ArrayList<Node> closed = new ArrayList<Node>(100000);
		ArrayList<Node> Nodes = new ArrayList<Node>(100000);
		Node temp;
		Node temp1;
		
		if (heuristics == 1) {
		state.heuristics = 1; 
	// how to force sorting algorithm ??
		} else if (heuristics == 2){
			state.heuristics = 2;
		}
		open.add(state); 
		while (open.size() > 0) {			
//			if (Nodes.size() %1000 == 0 ) 
//				puts (temp);
//			if (Nodes.size() %500 == 0 ) {
//				puts (Nodes.size());
//			}
			temp = open.remove(0);

			closed.add(temp);
			
			if (temp.solved())
			{
				puts("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				ArrayList<Node> solution = new ArrayList<Node>();
				Node T = temp; 
				solution.add(T);
				while (T.parent != null) {
					T = T.parent;
					solution.add(T);
				}
				puts("--------------------------------------------------");
				puts("######################################\n");
				puts ("Solution length: " + (temp.moves.size()));
				puts ("Total Nodes Generated: " + (open.size() + closed.size() + Nodes.size()));
				puts ("Total Nodes Examined: " + closed.size());
				puts("\nPuzzle:");
				//puts(solution.get(solution.size() - 1));
				puts(state);
				puts("Goal:");
				state.printGoal();
				puts ("Steps:\n");
				int i = 1; 
				for (Rule R : temp.moves) {
					puts(i + ": " + R.name);
					i += 1; 
				}
				//solvePuzzle(temp.moves, state, orderedRules);
				Collections.reverse(solution);	
				for (Node N : solution) {
					puts(N);
				}
				return; 
			}
			
			for (Rule R : temp.applicable_Rules_list) {
				temp1 = R.action(temp, orderedRules);
				Nodes.add(temp1);
			}
			
			for (Node N : Nodes) {
				check = false; 
				if (open.isEmpty()){
					open.add(N);
					continue; 
				}
				check = false;
				for (Node O : open) {
					if (N.equal(O)) {
						check = true; break; 
					}
				}
				if (!check) {
					for (Node C : closed) {
						if (N.equal(C)) {
							check = true; break; 
						}
					}
				}
				if (!check) {
					open.add(N);
				}
			}
			if (heuristics == 1 || heuristics == 2) {
				ArrayList<Node> stuff= SortDistance(open);
				open = stuff; 
			}			
		}
	}
	public static ArrayList<Node> SortDistance (ArrayList<Node> list){
		Collections.sort(list);
		return list; 
	}
	public static void IterativeDFS(String[] files) {
		Node x = new Node();
		ArrayList<Rule> orderedRules = new ArrayList<Rule>();
		orderedRules.add(new Rule("up",1));
		orderedRules.add(new Rule("down",2));
		orderedRules.add(new Rule("left",3));
		orderedRules.add(new Rule("right",4));	
		ArrayList<Node> start = new ArrayList<Node>();
		ArrayList<Rule> end = new ArrayList<Rule>();
		Long examined[] = {(long)0};

		// loop over all files in folder
		for (int i = 0 ; i < files.length; ++i ) {
			examined[0] = (long)0;
			x = null; 
			System.gc();
			x = new Node();
			x.readFile(files[i]);	
			x.applicable_Rules(orderedRules);
			start.clear();
			end.clear();
			start.add(x);
			
			for (int j = 0 ; j < 100 ; j++ ) {
				examined[0] = (long)0;
				x.applicable_Rules(orderedRules);
				start = new ArrayList<Node>();
				end = new ArrayList<Rule>();
				start.add(x);
				end = Backtrack1(start, orderedRules, j+1 , 0, examined);
				puts(j+": Cumulative Nodes Examined = " +examined[0] );
				if (end != null) {
					break; 
				}
				
			}
			Collections.reverse(end);
			//puts (end.size());
			puts("######################################\n");
			puts ("Total Nodes Examined: " + examined[0]);
			puts ("Path To Solution length: " + end.size());
			puts("\nPuzzle:");
			puts(x);
			puts("Goal:");
			x.printGoal();	
			puts ("Steps:\n");
			
			for (Rule rule : end) {
				puts ((end.indexOf(rule) +1)+ ": "+rule.name);	
			}
			
			puts("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			solvePuzzle(end, x, orderedRules);
		}
	}
	
	public static void DFS(String[] files, Integer[] bound){
		Node x = new Node();
		ArrayList<Rule> orderedRules = new ArrayList<Rule>();
		orderedRules.add(new Rule("up",1));
		orderedRules.add(new Rule("down",2));
		orderedRules.add(new Rule("left",3));
		orderedRules.add(new Rule("right",4));
		
		ArrayList<Node> start = new ArrayList<Node>();
		ArrayList<Rule> end = new ArrayList<Rule>();
		
		Long examined[] = {(long)0};
		
		// loop over all files in folder
		for (int i = 0 ; i < files.length; ++i ) {
			
			examined[0] = (long)0;
			x = new Node();
			x.readFile(files[i]);	
			x.applicable_Rules(orderedRules);
			start.clear();
			end.clear();
			start.add(x);
			puts(x);
			end = Backtrack1(start, orderedRules, bound[i] , 0, examined);
			Collections.reverse(end);
			//puts (end.size());
			puts("######################################\n");
			puts ("Total Nodes Examined: " + examined[0]);
			puts ("Path To Solution length: " + end.size());
			puts("\nPuzzle:");
			puts(x);
			puts("Goal:");
			x.printGoal();	
			puts ("Steps:\n");
			
			for (Rule rule : end) {
				puts ((end.indexOf(rule) +1)+ ": "+rule.name);	
			}
			
			puts("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			solvePuzzle(end, x, orderedRules);
		}
		
	}
	// Backtrack1 function
	// NOTE: bound >+ MIN(number of moves to reach the solution)
	// Backtrack1(dataList, orderedRules , bound, count, examined[])
	public static ArrayList<Rule> Backtrack1(ArrayList<Node> dataList, ArrayList<Rule> orderedRules , int bound, int count, Long examined[]){
		//https://stackoverflow.com/questions/34669/how-to-keep-a-things-done-count-in-a-recursive-algorithm-in-java

		Node data = dataList.remove(0);
		ArrayList<Rule> temp = new ArrayList<Rule> ();
		if (member(data, dataList)) {
			return null; 
		}

		if (data.solved()) { 		
			return new ArrayList<Rule>();
		}	
		
		if (deadEnd(data, dataList, orderedRules ) ) {
			return null;
		}
		
		if (count > bound) {
			return null;
		}
		
		for (Rule currentRule : data.applicable_Rules_list) {
			if (data.applicable_Rules_list.size() == 0 ) {return null;}
			examined[0] += 1;
			dataList.add(currentRule.action(data, orderedRules));
			count += 1; 
			temp = Backtrack1(dataList, orderedRules , bound, count, examined);
			
			if (temp == null) {
				count -= 1 ; 
				continue;
			}
			temp.add(currentRule);
			return temp;
		}		
		return null;
	}
	
	public static void solvePuzzle(ArrayList<Rule> Rules, Node state, ArrayList<Rule> orderedRules) {	
		
		puts("Steps :");
		Node currentState = state;
		puts(currentState); 
		for (Rule r : Rules) {
			currentState = r.action(currentState, orderedRules);
			puts(currentState); 
		}
	}
	
	// Walk through all of the applicable_Rules_list 
	// (updated for every change to a node) and see if they 
	// are a member of the list. 
	public static Boolean deadEnd(Node data, ArrayList<Node> dataList, ArrayList<Rule> orderedRules ) {
		
		int countFails = 0 ; 
		for (Rule move : data.applicable_Rules_list){
			if (member(move.action(data, orderedRules), dataList)) {
				countFails += 1 ; 
			}
		}
		if (countFails == data.applicable_Rules_list.size() && data.applicable_Rules_list.size() != 0) {
			return true; 
		}
		return false; 
	}
	
	// walk all of the nodes in current list
	// if two of them have the same 'state' we consider them equal 
	// and return true
	public static Boolean member(Node data , ArrayList<Node> dataList) {
		for (Node node : dataList) {
			if (data.equal(node)){
				return true; 
			}
		}
		return false; 
	}
	
	public static <T> void puts (T arg) {
		System.out.println(arg);
	}
}